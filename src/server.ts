import { createMockServer } from "./createMockServer";

type businessOrder = Array<{ id: string }>;

interface Routes {
  "/businessOrder": businessOrder;
}

createMockServer<Routes>({
  "/businessOrder": [
    {
      id: "wx_vip",
    },
  ],
})
