import { createServer } from "http";

export const createMockServer = <T extends object>(routes: T) => {
  const routePaths = Object.keys(routes);
  const server = createServer((req, res) => {
    const target = req.url;
    if (routePaths.includes(target)) {
      const template = routes[target];
      res.end(JSON.stringify(template));
    } else {
      res.end("404");
    }
  });
  // start
  server.listen(3000, () => console.log(`http://localhost:3000`));
};
