```ts
type businessOrder = Array<{ id: string }>;

interface Routes {
  "/businessOrder": businessOrder;
}

createMockServer<Routes>({
  "/businessOrder": [
    {
      id: "wx_vip",
    },
  ],
});
```
